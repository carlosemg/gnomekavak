//
//  ConexionCore.swift
//  Gnome
//
//  Created by CarlosEMG on 6/24/19.
//  Copyright © 2019 TechM. All rights reserved.
//

import UIKit

enum REQUEST_TYPE {
    case GET, POST, DELETE, UPDATE
    
    fileprivate func getRaWRequestType() -> String {
        switch self {
        case .GET:
            return "GET"
        case .POST:
            return "POST"
        case .UPDATE:
            return "UPDATE"
        default:
            return "DELETE"
        }
    }
}

struct RequestInfo {
    var urlBase:String
    var requestType:REQUEST_TYPE
    var needsResponse = false
}

class ConexionCore: NSObject {
    
    static let sharedInstance = ConexionCore()
    
    fileprivate var session:URLSession
    
    private override init() {
        self.session = ConexionCore.configureSession()
        super.init()
    }

    fileprivate static func configureSession () -> URLSession {
        let configuration = URLSessionConfiguration.default
        configuration.allowsCellularAccess = true
        configuration.httpMaximumConnectionsPerHost = 1
        configuration.httpShouldSetCookies = false
        configuration.httpCookieAcceptPolicy = .never
        return URLSession(configuration: configuration)
    }
    
    func sendSimpleRequest( info:RequestInfo, andCompletion completion:((_ succes:Bool, _ rawResponse:String?)->())? ) {
        guard let url = URL(string: info.urlBase) else {
            completion?(false, nil)
            return
        }
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30)
        request.httpMethod = info.requestType.getRaWRequestType()
        let task = self.session.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            guard error == nil || data != nil else {
                completion?(false, nil)
                return
            }
            guard let infoString = String(data: data!, encoding: .utf8) else {
                completion?(false, nil)
                return
            }
            completion?(true, info.needsResponse ? infoString : nil)
        }
        
        task.resume()
    }
    
    func downloadImgResource( info:RequestInfo, andCompletion completion:((_ succes:Bool, _ image:UIImage?)->())? ) {
        guard let url = URL(string: info.urlBase) else {
            completion?(false, nil)
            return
        }
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30)
        request.httpMethod = info.requestType.getRaWRequestType()
        let task = self.session.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            guard error == nil || data != nil else {
                completion?(false, nil)
                return
            }
           
            guard let image =  UIImage(data: data!) else {
                completion?(false, nil)
                return
            }
            completion?(true, info.needsResponse ? image : nil)
        }
        
        task.resume()
    }
    
}
