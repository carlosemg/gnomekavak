//
//  ConexionService.swift
//  Gnome
//
//  Created by CarlosEMG on 6/24/19.
//  Copyright © 2019 TechM. All rights reserved.
//

import UIKit

fileprivate let BASE_GNOME_URL = "https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json"

protocol RequestGnomoDelegate {
    
    func finishRequest()
    
}

struct InfoToSearch {
    var name = ""
    var age = ""
    var genre = 2
    var hair = ""
    var job = ""
    var height = ""
    var weight = ""
}

class GnomoService: NSObject {
    
    static let sharedInstance = GnomoService()
    
    fileprivate var brastlewark:[Brastlewark] = []
    var currentBrastlewark:[Brastlewark] = []
    fileprivate var listImgBrastlewark:[String: UIImage]?
    var listJobs:[String]?
    
    var delegate:RequestGnomoDelegate?
    
    func requestInfoGnomes() {
        let requestGnome = RequestInfo(urlBase: BASE_GNOME_URL, requestType: REQUEST_TYPE.GET, needsResponse: true)
        ConexionCore.sharedInstance.sendSimpleRequest(info: requestGnome) { [weak self]
            (success, info) in
            if success {
                self?.parseInfoResponse(info: info!)
            } else {
                self?.delegate?.finishRequest()
            }
        }
    }
    
    fileprivate func parseInfoResponse( info: String) {
        if let jsonData = info.data(using: .utf8)
        {
            if let decode = try? JSONDecoder().decode(BaseGnome.self, from: jsonData) {
                brastlewark = decode.brastlewark
                currentBrastlewark = brastlewark
                listImgBrastlewark = [:]
            }
        }
        makeListJobs()
        asignGenre()
        delegate?.finishRequest()
    }
    
    fileprivate func makeListJobs() {
        listJobs = []
        brastlewark.forEach { (current) in
            current.professions.forEach({ (job) in
                if !listJobs!.contains(job) {
                    listJobs!.append(job)
                }
            })
        }
    }
    
    fileprivate func asignGenre() {
        brastlewark.forEach { (current) in
            let name = current.name.lowercased()
            current.isFemale = name.starts(with: "a") || name.starts(with: "e") || name.starts(with: "i") || name.starts(with: "o") || name.starts(with: "u")
        }
    }
    
    func requestImgGnome(urlString:String, andCompletion completion:@escaping (_ image:UIImage?)->() ) {
        let urlToSend = urlString.replacingOccurrences(of: "http://", with: "https://")
        if listImgBrastlewark![urlToSend] != nil {
            DispatchQueue.main.async {
                completion(self.listImgBrastlewark?[urlToSend])
            }
        } else {
            let requestGnome = RequestInfo(urlBase: urlToSend, requestType: REQUEST_TYPE.GET, needsResponse: true)
            ConexionCore.sharedInstance.downloadImgResource(info: requestGnome) { [weak self]
                (success, image) in
                self?.listImgBrastlewark?[urlToSend] = image
                DispatchQueue.main.async {
                    completion(image)
                }
            }
        }
    }
    
    func applyFilter( info:InfoToSearch ) {
        currentBrastlewark = brastlewark
        if info.name != "" {
            currentBrastlewark = currentBrastlewark.filter({ (currentBrastlewark) -> Bool in
                currentBrastlewark.name.uppercased().contains(info.name.uppercased())
            })
        }
        if info.hair != "" {
            currentBrastlewark = currentBrastlewark.filter({ (currentBrastlewark) -> Bool in
                currentBrastlewark.hairColor.uppercased().contains(info.hair.uppercased())
            })
        }
        if info.height != "" {
            let h = Float(info.height)!
            if h > 0 {
                currentBrastlewark = currentBrastlewark.filter({ (currentBrastlewark) -> Bool in
                    currentBrastlewark.height >= (h - 5) && currentBrastlewark.height <= (h + 5)
                })
            }
        }
        if info.weight != "" {
            let w = Float(info.weight)!
            if w > 0 {
                currentBrastlewark = currentBrastlewark.filter({ (currentBrastlewark) -> Bool in
                    currentBrastlewark.weight >= (w - 5) && currentBrastlewark.weight <= (w + 5)
                })
            }
        }
        if info.genre < 2  {
            currentBrastlewark = currentBrastlewark.filter({ (currentBrastlewark) -> Bool in
                currentBrastlewark.isFemale == (info.genre == 1)
                
            })
        }
        if info.age != "" {
            let a = Int(info.age)!
            if a > 0 {
                currentBrastlewark = currentBrastlewark.filter({ (currentBrastlewark) -> Bool in
                    currentBrastlewark.age >= (a - 5) && currentBrastlewark.age <= (a + 5)
                })
            }
        }
        if info.job != "" {
            currentBrastlewark = currentBrastlewark.filter({ (currentBrastlewark) -> Bool in
                currentBrastlewark.professions.contains(info.job.capitalized)
            })
        }
    }

}
