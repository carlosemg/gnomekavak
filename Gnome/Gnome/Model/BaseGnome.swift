//
//  BaseGnome.swift
//  Gnome
//
//  Created by CarlosEMG on 6/24/19.
//  Copyright © 2019 TechM. All rights reserved.
//

import UIKit

class Brastlewark: Codable {
    
    var idElement:Int
    var name:String
    var urlImage:String
    var age:Int
    var weight:Float
    var height:Float
    var hairColor:String
    var professions:[String]
    var friends:[String]
    var isFemale = false
    
    var description: String {
        return "Id:\(idElement), Name:\(name), Age:\(age), Weight:\(weight), Height:\(height), Hair Color:\(hairColor)"
    }
    
    init() {
        self.idElement = 0
        self.name = ""
        self.urlImage = ""
        self.age = 0
        self.weight = 0
        self.height = 0
        self.hairColor = ""
        self.professions = []
        self.friends = []
    }
    
    enum CodingKeys:String, CodingKey {
        case idElement = "id"
        case name = "name"
        case urlImage = "thumbnail"
        case age = "age"
        case weight = "weight"
        case height = "height"
        case hairColor = "hair_color"
        case professions = "professions"
        case friends = "friends"
    }
}

class BaseGnome: Codable {
    
    var brastlewark:[Brastlewark]
    
    init() {
        self.brastlewark = []
    }
    
    enum CodingKeys:String, CodingKey {
        case brastlewark = "Brastlewark"
    }
    
}
