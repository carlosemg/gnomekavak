//
//  HomeController.swift
//  Gnome
//
//  Created by CarlosEMG on 6/25/19.
//  Copyright © 2019 TechM. All rights reserved.
//

import UIKit

class HomeController: UIViewController, RequestGnomoDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if GnomoService.sharedInstance.currentBrastlewark.count == 0 {
            GnomoService.sharedInstance.delegate = self
            GnomoService.sharedInstance.requestInfoGnomes()
        }
    }
    
    func finishRequest() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "listGnomo", sender: self)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
