//
//  SearchController.swift
//  Gnome
//
//  Created by TechM User on 26/06/19.
//  Copyright © 2019 TechM. All rights reserved.
//

import UIKit

protocol SearchGnomeDelegate {
    func endSearch()
}


class SearchController: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var typeSearchSegment: UISegmentedControl!
    @IBOutlet weak var genreSegment: UISegmentedControl!
    @IBOutlet weak var jobsTable: UITableView!
    @IBOutlet weak var currentJob: UIButton!
    
    fileprivate var lisJobs:[String] = []
    fileprivate var currentInfo = InfoToSearch(name: "", age: "", genre: 2, hair: "", job: "", height: "", weight: "")
    var searchDelegate:SearchGnomeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lisJobs = GnomoService.sharedInstance.listJobs ?? []
        lisJobs.insert("Todos", at: 0)
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        jobsTable.layer.cornerRadius = 5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureInitial()
    }
    
    @IBAction func showListJob() {
        currentJob.alpha = 0
        jobsTable.isHidden = false
    }
    
    @IBAction func updateSegment(_ sender: Any) {
        searchBar.resignFirstResponder()
        switch typeSearchSegment.selectedSegmentIndex {
        case 0://nombre
            searchBar.keyboardType = .namePhonePad
            searchBar.text = currentInfo.name
            break
        case 1://edad
            searchBar.keyboardType = .numberPad
            searchBar.text = currentInfo.age
            break
        case 2://color cabello
           searchBar.keyboardType = .namePhonePad
           searchBar.text = currentInfo.hair
            break
        case 3://peso
            searchBar.keyboardType = .decimalPad
            searchBar.text = currentInfo.weight
            break
        default://altura
            searchBar.keyboardType = .decimalPad
            searchBar.text = currentInfo.height
        }
        searchBar.becomeFirstResponder()
    }
    
    @IBAction func search() {
        searchBar.resignFirstResponder()
        currentInfo.job = currentJob.title(for: .normal)! == "Todos" ? "" : currentJob.title(for: .normal)!
        currentInfo.genre = genreSegment.selectedSegmentIndex
        GnomoService.sharedInstance.applyFilter(info: currentInfo)
        searchDelegate?.endSearch()
    }
    
    fileprivate func configureInitial() {
        currentJob.setTitle("Todos", for: .normal)
        typeSearchSegment.selectedSegmentIndex = 0
        genreSegment.selectedSegmentIndex = 2
        jobsTable.reloadData()
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        switch typeSearchSegment.selectedSegmentIndex {
        case 0://nombre
            currentInfo.name = searchText
            break
        case 1://edad
            currentInfo.age = searchText
            break
        case 2://color cabello
            currentInfo.hair = searchText
            break
        case 3://peso
            currentInfo.weight = searchText
            break
        default://altura
            currentInfo.height = searchText
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.search()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        currentJob.setTitle(lisJobs[0], for: .normal)
        currentInfo = InfoToSearch(name: "", age: "", genre: 2, hair: "", job: "", height: "", weight: "")
        self.search()
    }
}

extension SearchController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lisJobs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SimpleCell", for: indexPath)
        cell.textLabel?.text = lisJobs[indexPath.row]
        return cell
    }
}

extension SearchController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        jobsTable.isHidden = true
        currentJob.alpha = 1
        currentInfo.job = lisJobs[indexPath.row]
        currentJob.setTitle(lisJobs[indexPath.row], for: .normal)
    }
}
