//
//  ListGnomoController.swift
//  Gnome
//
//  Created by CarlosEMG on 6/25/19.
//  Copyright © 2019 TechM. All rights reserved.
//

import UIKit

class ListGnomoController: UIViewController {
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var nameAgeLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var hightLabel: UILabel!
    @IBOutlet weak var hairLabel: UILabel!
    @IBOutlet weak var gnomosTable: UITableView!
    @IBOutlet weak var friendContainerView: UIView!
    @IBOutlet weak var friendButton: UIButton!
    @IBOutlet weak var conbstraintFilter: NSLayoutConstraint!
    
    fileprivate var currentGnomo:Brastlewark?
    fileprivate var listGnomo:[Brastlewark] = []
    fileprivate weak var friendController:FriendLisController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.friendButton.setTitle("Amigos y Oficios", for: .normal)
        self.friendButton.setTitle("Ver Listado", for: .selected)
        self.conbstraintFilter.constant = self.view.frame.width
        self.photo.layer.cornerRadius = 3.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        listGnomo = GnomoService.sharedInstance.currentBrastlewark
        gnomosTable.reloadData()
        if listGnomo.count > 0 {
            currentGnomo = listGnomo[0]
            gnomosTable.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
            fillCurrentGnomo()
        }
    }
    
    fileprivate func fillCurrentGnomo() {
        self.nameAgeLabel.text = "\(currentGnomo?.name ?? "") (\(currentGnomo?.age ?? 0) - \(currentGnomo!.isFemale ? "M" : "H"))"
        self.nameAgeLabel.textColor = currentGnomo!.isFemale ? UIColor.magenta : UIColor.black
        self.heightLabel.text = "\(currentGnomo?.height ?? 0)"
        self.hightLabel.text = "\(currentGnomo?.weight ?? 0)"
        self.hairLabel.text = "\(currentGnomo?.hairColor ?? "")"
        GnomoService.sharedInstance.requestImgGnome(urlString: currentGnomo?.urlImage ?? "") { (image:UIImage?) in
            if image != nil {
                self.photo.image = image
            }
        }
    }
    
    @IBAction func showSearch(_ sender: UIButton) {
        UIView.animate(withDuration: 2.0, delay: 0.5, options: .curveEaseInOut, animations: {
            self.conbstraintFilter.constant = sender.tag == 2 ? self.view.frame.width : 0
        }, completion: nil)
    }
    
    @IBAction func displayFriend() {
        UIView.animate(withDuration: 1.0, animations: {
            self.friendContainerView.alpha = self.friendContainerView.alpha == 1 ? 0 : 1
            self.gnomosTable.alpha = self.gnomosTable.alpha == 1 ? 0 : 1
            if !self.friendButton.isSelected {
                self.updateListFriends()
            }
            self.friendButton.isSelected = !self.friendButton.isSelected
        }, completion: nil)
    }
    
    fileprivate func updateListFriends() {
        friendController?.listFriends = currentGnomo?.friends ?? []
        friendController?.listJobs = currentGnomo?.professions ?? []
        friendController?.updateContent()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showFriends" {
            friendController = segue.destination as? FriendLisController
        } else if segue.identifier == "search" {
            (segue.destination as? SearchController)?.searchDelegate = self
        }
    }
    
}

extension ListGnomoController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentGnomo = listGnomo[indexPath.row]
        UIView.animate(withDuration: 1.0, animations: {
            self.conbstraintFilter.constant =  self.view.frame.width
        }, completion: nil)
        fillCurrentGnomo()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.0
    }
}

extension ListGnomoController:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listGnomo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SimpleCell", for: indexPath)
        cell.textLabel?.text = listGnomo[indexPath.row].name
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.textColor = listGnomo[indexPath.row].isFemale ? UIColor.magenta : UIColor.black
        return cell
    }
    
}

extension ListGnomoController:SearchGnomeDelegate {
    func endSearch() {
        self.viewWillAppear(true)
    }
}
