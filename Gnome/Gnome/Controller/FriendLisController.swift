//
//  FriendLisController.swift
//  Gnome
//
//  Created by TechM User on 25/06/19.
//  Copyright © 2019 TechM. All rights reserved.
//

import UIKit

class FriendLisController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var listFriends:[String] = []
    var listJobs:[String] = []
    
    @IBOutlet private weak var friendTable: UITableView!
    @IBOutlet private weak var jobTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func updateContent() {
        friendTable.reloadData()
        jobTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20.0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return tableView == friendTable ? "Amigos" : "Oficios"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == friendTable ? listFriends.count : listJobs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SimpleCell", for: indexPath)
        cell.textLabel?.text = tableView == friendTable ? listFriends[indexPath.row] : listJobs[indexPath.row]
        cell.textLabel?.numberOfLines = 2
        cell.layer.cornerRadius = 7.0
        return cell
    }

}
